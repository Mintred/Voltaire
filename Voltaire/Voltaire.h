/*
 * File Name: Voltaire.h
 * Author(s): Pelanyo Kamara
 */

#pragma once

#ifdef _WIN32
#include <Windows.h>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <Dbghelp.h>

#ifdef VOLTAIRE_DLL_EXPORT
#define VOLTAIRE_EXPORT __declspec(dllexport) 
#else
#define VOLTAIRE_EXPORT __declspec(dllimport) 
#endif
#endif