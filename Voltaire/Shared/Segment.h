/*
 * File Name: Segment.h
 * Author(s): Robert S (moded)
 */

#pragma once

#include "..\Voltaire.h"

class VOLTAIRE_EXPORT CSegment
{
	public:
		CSegment(std::string Segment, const char *Module, bool Custom);

		std::string Segment;

		int BaseAddress = 0;
		int Size = 0;
		int ModuleBase = 0;
		int Hash = 0;

		bool Custom;
};