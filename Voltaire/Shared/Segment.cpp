/*
 * File Name: Segment.cpp
 * Author(s): Robert S (moded)
 */

#include "Segment.h"

CSegment::CSegment(std::string Segment, const char *Module, bool Custom)
{
	if (Custom)
	{
		this->Segment = Segment;
		this->Custom = true;
		this->ModuleBase = (int)GetModuleHandle((LPCSTR)Module);
		return;
	}

	int* dllImageBase = 0;
	dllImageBase = (int*)GetModuleHandle((LPCSTR)Module);
	IMAGE_NT_HEADERS *pNtHdr = ImageNtHeader((PVOID)dllImageBase);
	IMAGE_SECTION_HEADER *pSectionHdr = (IMAGE_SECTION_HEADER *)(pNtHdr + 1);
	for (int i = 0; i < pNtHdr->FileHeader.NumberOfSections; i++)
	{
		char *name = (char*)pSectionHdr->Name;
		if (memcmp(name, Segment.c_str(), Segment.length()) == 0)
		{
			this->Segment = Segment;
			this->ModuleBase = (int)dllImageBase;
			this->BaseAddress = (int)dllImageBase + pSectionHdr->VirtualAddress;
			this->Size = pSectionHdr->Misc.VirtualSize;
			break;
		}
		pSectionHdr++;
	}
}
