/*
 * File Name: ProgramMemoryChecker.h
 * Author(s): Robert S (moded)
 */

#pragma once

#include "..\Shared\Segment.h"

namespace Voltaire
{
	namespace Security
	{
		#define CONST1 0x6E3AB44F 
        #define CONST2 0x13002AE1
        #define CONST3 0x92A27283
        #define CONST4 0x006314E0

        #define PROGRAM_MEMORYCHECK_SIZE 256

		class VOLTAIRE_EXPORT ProgramMemoryCheck
		{
			public:
				void AddSegment(CSegment*);
            	void InitMemoryCheck(std::vector <std::string> segments);
            	void AddCustomRegion(const std::string &name, int baseaddress, int size, const char* m);
            	void DeleteCustomRegion(std::string name);

				inline int GetTick() { return RunTick; };

				int getRegionHashByName(const std::string &name);

				inline std::vector <int> GetSavedHashes() {
	            	return this->savedHashes;
	            };

				inline std::vector <int> GetLastRunHashes() {
	            	std::vector <int> Hashes;
	               	for (int i = 0; i < Segments.size(); ++i) {
		        	    Hashes.push_back(Segments[i]->Hash);
	               	}
		        	return Hashes;
	            }

				int Step();
				int GetLastCompletedHash();
			private:
				int *MemoryCheckPointer = nullptr;
				int CompleteHash = 0;
				int RunTick = 0;

				std::vector <int> savedHashes;
	            std::vector <CSegment*> Segments;
				
				int StepHasher(int* Size, int BaseAddress);
		};

		typedef int (ProgramMemoryCheck::*MemoryCheckStep)(int*, int);
	}
}