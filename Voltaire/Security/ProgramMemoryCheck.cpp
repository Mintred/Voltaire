/*
 * File Name: ProgramMemoryChecker.cpp
 * Author(s): Robert S (moded)
 */

#include "ProgramMemoryCheck.h"

namespace Voltaire
{
	namespace Security
	{
		void ProgramMemoryCheck::InitMemoryCheck(std::vector<std::string> Segments)
		{
			srand(std::time(NULL));
			for (size_t i = 0; i < Segments.size(); ++i)
			{
				AddSegment(new CSegment(Segments[i], 0, false));
			}

	    	int(__thiscall ProgramMemoryCheck::* pmcStep)(int*, int) = &ProgramMemoryCheck::StepHasher;
	        this->MemoryCheckPointer = (int*&)pmcStep;

			Step();
			for (size_t i = 0; i < this->Segments->size(); ++i)
			{
				SavedHashes->push_back(this->Segments->at(i)->Hash);
			}
			RunTick = -1;
		}

		void ProgramMemoryCheck::AddCustomRegion(std::string Name, int BaseAddr, int Size, const char *Module) 
		{
			CSegment* customRegion = new CSegment(Name, Module, true);
			customRegion->BaseAddress = BaseAddr;
			customRegion->Size = Size;
			AddSegment(customRegion);
		}

		void ProgramMemoryCheck::DeleteCustomRegion(std::string Name)
		{
			for (int i = 0; i < Segments.size(); ++i)
			{
				if (Segments[i]->Segment == Name)
				{
					Segments.erase(Segments.begin() + i);
					SavedHashes.erase(SavedHashes.begin() + i);
				}
			}
		}

		int ProgramMemoryCheck::getRegionHashByName(const std::string &name)
		{
    	    for (int i = 0; i < this->Segments.size(); ++i) {
	        	if (this->Segments[i]->segment == name) {
			       return this->Segments[i]->Hash;
	    	    }
        	}
        	return 0;
        }

		int ProgramMemoryCheck::Step()
		{
			DWORD oldprotection = 0;
			int* dynamicMemoryCheck = new int[2048];
			int* pMemoryCheck = MemoryCheckPointer;
			if (!VirtualProtect(dynamicMemoryCheck, PROGRAM_MEMORYCHECK_SIZE, PAGE_EXECUTE_READWRITE, &oldprotection))
			{
				return 0;
			}
			else
			{
				for (int i = 0; i < PROGRAM_MEMORYCHECK_SIZE; ++i)
				{
					dynamicMemoryCheck[i] = (*(int*)pMemoryCheck);
					pMemoryCheck += 1;
				}
				MemoryCheckStep s_func = reinterpret_cast<MemoryCheckStep&>(dynamicMemoryCheck);

				int completeHash = 0;
				for (int i = 0; i < Segments->size(); ++i)
				{

					int* Size = (int*)Segments->at(i)->Size;
					int BaseAddress = Segments->at(i)->BaseAddress;
					int regionHash = (this->*s_func)(Size, BaseAddress);
					Segments->at(i)->Hash = regionHash;

					if (!Segments->at(i)->Custom)
					{
						completeHash += regionHash;
					}
				}
				CompleteHash = completeHash;

				RunTick++;

				pMemoryCheck = MemoryCheckPointer;
				for (int i = 0; i < PROGRAM_MEMORYCHECK_SIZE; ++i)
				{
					dynamicMemoryCheck[i] = rand() % 255;
					pMemoryCheck += 1;
				}

				delete[] dynamicMemoryCheck;
				return 1;
			}
			return 0;
		}

		void ProgramMemoryCheck::AddSegment(CSegment* Segment)
		{
			Segments->push_back(Segment);
		}

		int ProgramMemoryCheck::StepHasher(int* Size, int BaseAddress)
		{
			int regionHash = 0;
			for (int* a = 0; a < Size; a += 4) {
				int b1 = (*(int*)(BaseAddress + (int)a));
				regionHash += (b1 * CONST1) + b1 - (BaseAddress + (int)a);
				int b2 = (*(int*)(BaseAddress + (int)a + 4));
				regionHash -= b2 - CONST2;
				int b3 = (*(int*)(BaseAddress + (int)a + 8));
				regionHash += (b3 ^ CONST3) - b3;
				int b4 = (*(int*)(BaseAddress + (int)a + 12));
				regionHash -= (b4 && 0x03) + b4;
			}
			return regionHash;
		}

		int ProgramMemoryCheck::GetLastCompletedHash() {
			return CompleteHash;
		}
	}
}